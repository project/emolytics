<?php
/**
 * @file
 * Contains \Drupal\Emolytics\Form\EmolyticsForm.
 */
namespace Drupal\Emolytics\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class EmolyticsForm extends FormBase {

	/**
	 * {@inherithoc}
	 */
	public function getFormId(){
		return 'emolytics_settings';
	}

	/**
	 * {@inherithoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state){
		$form['emolyticsId'] = array(
			'#type' => 'textfield',
			'#title' => t('Emolytics id'),
			'#default_value' => \Drupal::config('emolytics.settings')->get('emolytics_userid'),
			'#size' => 15,
			'#maxlength' => 15,
			'#description' => t("Emolytics id"),
			'#required' => TRUE,
		);

		$form['show'] = array(
			'#type' => 'submit',
			'#value' => $this->t('Save settings'),
			'#button_type' => 'primary',
		);

		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		$emolyticsId = $form_state->getValue("emolyticsId");
		if (strlen($emolyticsId) !== 15) {
			$form_state->setErrorByName('emolyticsId', $this->t('Value for emolyticsId must be 15 chars.'));
		}

		if (!preg_match("/[a-z0-9]{15}/i", preg_quote($emolyticsId, "\\"))) {
			$form_state->setErrorByName('emolyticsId', $this->t('Value for emolyticsId can contain only chars and numbers.'));
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		\Drupal::service('config.factory')
			->getEditable('emolytics.settings')
			->set('emolytics_userid', $form_state->getValue("emolyticsId"))
			->save()
		;
		drupal_set_message(
			$this->t('Your emolyticsId is @emolyticsId', [
				'@emolyticsId' => $form_state->getValue("emolyticsId")
			])
		);
	}
}
